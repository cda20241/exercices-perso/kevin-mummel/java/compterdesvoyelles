import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // On créer une variable de type scanner pour lire l'entrée de l'utilisateur
        Scanner inputScanner = new Scanner(System.in);
        // On affiche "Entrez une phrase svp : "
        System.out.print("Entrez une phrase svp : ");
        // Enregistre l'entrée utilisateur dans la variable "phrase"
        String phrase = inputScanner.nextLine();
        // stoper l'enregistrement de l'utilisateur
        inputScanner.close();
        // On affiche le nombre de voyelle dans la variable "phrase"
        System.out.println("Il y a " + nombreVoyelles(phrase) + " voyelles dans la phrase.");
        // On créer un dictionnaire avec les voyelles associé à leurs nombre
        Map<Character, Integer> voyelleCount = nombreChaqueVoyelle(phrase);
        //On affiche le contenu du dictionnaire "voyelleCount"
        System.out.println(voyelleCount);
    }


    public static int nombreVoyelles(String phrase) {
        // Créer une nouvelle variable de type chaine de caractères sans accents
        String phraseSansAccents = removeAccents(phrase);
        // Initialiser la variable entière "nbVoyelles" à 0
        int nbVoyelles = 0;
        // Créer la variable de type chaine de caractères et lui assigner "aeiouy"
        String voyelles = "aeiouy";
        // Boucle qui parcour les caractère un à un pour les convertir en minuscule
        for (char lettre : phraseSansAccents.toLowerCase().toCharArray()) {
            // Si "lettre" est une voyelle alors
            if (voyelles.contains(String.valueOf(lettre))) {
                // nbVoyelles++ est incrémenté de 1
                nbVoyelles++;
            }
        }
        return nbVoyelles;
    }

    public static Map<Character, Integer> nombreChaqueVoyelle(String phrase) {
        // Créer une nouvelle variable de type chaine de caractères sans accents
        String phraseSansAccents = removeAccents(phrase);
        // Rajoute une nouvelle instance du dictionnaire "compteVoyelle"
        Map<Character, Integer> compteVoyelles = new HashMap<>();
        // Créer la variable de type chaine de caractères et lui assigner "aeiouy"
        String voyelles = "aeiouy";
        // Boucle qui parcour les caractère un à un pour les convertir en minuscule
        for (char lettre : phraseSansAccents.toLowerCase().toCharArray()) {
            // Si "lettre" est une voyelle et qu'elle est déjà dans le dictionnaire "compteVoyelles"
            if (voyelles.contains(String.valueOf(lettre)) && !compteVoyelles.containsKey(lettre)) {
                // on ajoute cette lettre si elle n'existe pas dans le dictionnaire "compteVoyelles" avec une valeur de 1
                compteVoyelles.put(lettre, 1);
                // Sinon Si "lettre" est une voyelle et qu'elle est déjà dans le dictionnaire "compteVoyelles"
            } else if (voyelles.contains(String.valueOf(lettre)) && compteVoyelles.containsKey(lettre)) {
                // On incrémente cette lettre
                compteVoyelles.put(lettre, compteVoyelles.get(lettre) + 1);
            }
        }
        return compteVoyelles;
    }

    public static String removeAccents(String input) {
        // Utiliser la méthode "normalize" avec la classe "Normalizer" qui utilise la normalisation importée "Normalizer.Form.NFD"
        // Cette méthode sépare les accents de leurs caractères
        return Normalizer.normalize(input, Normalizer.Form.NFD)
                // Remplacer les accents "\p{InCombiningDiacriticalMarks}+" par du vide
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
